class AddFileField < ActiveRecord::Migration
  def up
    add_attachment :object_generator_d_example_items, :avatar
  end

  def down
    remove_attachment :object_generator_d_example_items, :avatar
  end
end
