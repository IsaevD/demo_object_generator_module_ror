class CreateObjectGeneratorDExampleItems < ActiveRecord::Migration
  def change
    create_table :object_generator_d_example_items do |t|
      t.string :text_field
      t.string :password_field
      t.text :text_area_field
      t.integer :collection_field
      t.timestamps
    end
  end
end
