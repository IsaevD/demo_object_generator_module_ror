module ObjectGeneratorD
  module FormGeneratorHelper

    def render_text_field(form, field_name, field_label = nil)
      render "object_generator_d/form_generator/text_field", form: form, field_name: field_name, field_label: field_label
    end

    def render_password_field(form, field_name, field_label = nil)
      render "object_generator_d/form_generator/password_field", form: form, field_name: field_name, field_label: field_label
    end

    def render_collection_field(form, field_name, field_label = nil, items = nil, item_label = "name", is_blanked = false)
      render "object_generator_d/form_generator/collection_field",
        form: form,
        field_name: field_name,
        field_label: field_label,
        items: items,
        item_label: item_label,
        is_blanked: is_blanked
    end

    def render_textarea_field(form, field_name, field_label = nil)
      render "object_generator_d/form_generator/textarea_field", form: form, field_name: field_name, field_label: field_label
    end

    def render_wysiwyg_field(form, field_name, field_label = nil)
      render "object_generator_d/form_generator/wysiwyg_field", form: form, field_name: field_name, field_label: field_label
    end

    def render_file_field(form, field_name, field_label = nil)
      render "object_generator_d/form_generator/file_field", form: form, field_name: field_name, field_label: field_label
    end

    def render_field_label(form, field_name, field_label = nil)
      render "object_generator_d/form_generator/field_label", form: form, field_name: field_name, field_label: field_label
    end

  end
end