module ObjectGeneratorD
  module ConstructionGeneratorHelper

    def render_table(labels, items = nil)
      render "object_generator_d/construction_generator/table", labels: labels, items: items
    end

    def render_menu(items)
      render "object_generator_d/construction_generator/menu", items: items
    end

  end
end