module ObjectGeneratorD
  class ExampleItemsController < ApplicationController
    before_filter :init_variables

    def index
      all_item(@entity, nil, true, true)
    end

    def show
      get_item_by_id(@entity, params[:id])
    end

    def new
      new_item(@entity)
    end

    def edit
      get_item_by_id(@entity, params[:id])
    end

    def create
      create_item(@entity, @path, prepared_params(@param_name, @params_array))
    end

    def update
      update_item(@entity, @path, params[:id], prepared_params(@param_name, @params_array), @fields)
    end

    def destroy
      delete_item(@entity, @path, params[:id])
    end

    private

      def init_variables
        @entity = ExampleItem
        @param_name = :example_item
        @path = object_generator_d.example_items_path
        @fields = {
          :text_field => {
              :type => :string,
              :label => "text_field",
              :show_in_table => true
          },
          :text_area_field => {
              :type => :text,
              :label => "text_area_field",
              :show_in_table => false
          },
          :password_field => {
              :type => :string,
              :label => "password_field",
              :show_in_table => false
          },
          :collection_field => {
              :type => :collection,
              :label => "collection_field",
              :show_in_table => false
          },
          :avatar => {
              :type => :file,
              :label => "file_field",
              :show_in_table => false
          }
        }
        @params_array, @field_labels = @fields.map { |k,v| [k.to_s, v.to_s] }.transpose
      end

  end
end
