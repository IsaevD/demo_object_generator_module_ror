$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "object_generator_d/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "object_generator_d"
  s.version     = ObjectGeneratorD::VERSION
  s.authors     = "Denis Isaev"
  s.email       = "isaevdenismich@mail.ru"
  s.homepage    = "http://www.den-isaev.com"
  s.summary     = "Базовый модуль по генерации базовых HTML-конструкций"
  s.description = "Базовый модуль 0-ого уровня для генерации базовых HTML-конструкций"
  s.license     = "MIT"

  s.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.rdoc"]
  s.test_files = Dir["test/**/*"]

  s.add_dependency "rails", "~> 4.1.0"

  s.add_development_dependency "sqlite3"
end
